﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Module3_2.Tests
{
    public class Tests4
    {
        [TestFixture]
        public class Task4Tests
        {
            private Task4 _task4;

            [SetUp]
            public void SetUp()
            {
                _task4 = new Task4();
            }

            [Test]
            public void GetFibonacciSequence_ShoutReturn()
            {
                int[] result = _task4.GetFibonacciSequence(5);

                Assert.AreEqual(0, result[0]);
                Assert.AreEqual(1, result[1]);
                Assert.AreEqual(1, result[2]);
                Assert.AreEqual(2, result[3]);
                Assert.AreEqual(3, result[4]);
            }
        }
        [TestFixture]
        public class Task5Tests
        {
            private Task5 _task5;

            [SetUp]
            public void SetUp()
            {
                _task5 = new Task5();
            }

            [Test]
            public void ReverseNumber_ShoutReturn()
            {
                int result = _task5.ReverseNumber(567456247);
                Assert.AreEqual(742654765, result);
            }

        }
        [TestFixture]
        public class Task6Tests
        {
            private Task6 _task6;

            [SetUp]
            public void SetUp()
            {
                _task6 = new Task6();
            }

            [Test]
            public void UpdateElementToOppositeSign_ShoutReturn()
            {
                int[] result = _task6.UpdateElementToOppositeSign(new int[] { 1, 4, -8, 15, -10 });
                Assert.AreEqual(-1, result[0]);
                Assert.AreEqual(-4, result[1]);
                Assert.AreEqual(8, result[2]);
                Assert.AreEqual(-15, result[3]);
                Assert.AreEqual(10, result[4]);
            }
        }
        [TestFixture]
        public class Task7Tests
        {
            private Task7 _task7;

            [SetUp]
            public void SetUp()
            {
                _task7 = new Task7();
            }

            [Test]
            public void FindElementGreaterThenPrevious_ShoutReturn()
            {
                List<int> result = _task7.FindElementGreaterThenPrevious(new int[] { 3, 9, 8, 4, 5, 1});
                Assert.AreEqual(new List<int> { 9, 5 } , result);                             
            }
        }
    }
}