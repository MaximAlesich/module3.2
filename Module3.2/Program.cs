﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Module3_2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello world!");
        }
    }

    public class Task4
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            bool parsed = int.TryParse(input, out result) && result > 0;
            if (!parsed)
            {
                throw new Exception();
            }
            return parsed;
        }

        public int[] GetFibonacciSequence(int n)
        {
            if (n <= 0)
            {
                return null;
            }
            int[] fb = new int[n];
            fb[0] = 0;
            fb[1] = 1;
            for (int i = 2; i < n; i++)
            {
                fb[i] = fb[i - 1] + fb[i - 2];
            }
            return fb;
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            int result = 0;
            while (sourceNumber > 0)
            {
                result *= 10;
                result += sourceNumber % 10;
                sourceNumber /= 10;
            }
            return result;
        }
    }


    public class Task6
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            int[] useArry = new int[size];
            return useArry;
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            int[] useArray = new int[source.Length];
            for (int i = 0; i < source.Length; i++)
            {
                useArray[i] = source[i] * (-1);
            }
            return useArray;
        }
    }

    public class Task7
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            int[] useArry = new int[size];
            return useArry;
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            List<int> elementList = new List<int>();
            int index = 0;
            do
            {
                if (index < source.Length - 1 && source[index] < source[index + 1])
                {
                    elementList.Add(source[index + 1]);
                }
                index++;
            } while (index < source.Length);
            return elementList;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            throw new NotImplementedException();
        }
    }
}

